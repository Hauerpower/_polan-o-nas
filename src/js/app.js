import generalInit from './modules/general';
import slidersInit from './modules/sliders';
import menuInit from './modules/menu';
import galleryInit from './modules/wp-gallery';
import loadMoreInit from './modules/loadmore';

(function($){
	
	$(document).foundation();
		
	$(document).ready(function(){

		generalInit();
		slidersInit();
		menuInit();
		galleryInit();
		loadMoreInit(); 
		
		$('.navTrigger').click(function(){
			$(this).toggleClass('active')
			if($(this).hasClass('active')){
				$('.logo').css({
					opacity: 0,
					transition : '.5s ease-in-out'
				})
				$('.search-languages').css({
					opacity: 0,
					transition : '.5s ease-in-out'
				})
				$('header').css({
					position: 'fixed',
					zIndex: 205,
					width: '100%'
				})
				$('.mobile-navigation').addClass('active')
				setTimeout(function(){
					$(".mobile-navigation").css("display", 'block')
				},100)
				setTimeout(function(){
					$(".mobile-navigation").css({
						opacity: 1,
						transition: '.5s ease-in-out'
					})
				},150)
				$('.page-name-section').css('paddingTop', '93px')
			}else{
				$('.logo').css({
					opacity: 1
				})
				$('.search-languages').css({
					opacity: 1,
					transition : '.5s ease-in-out'
				})
				$('header').css({
					position: 'inherit',
					zIndex: 205
				})
				$('.mobile-navigation').removeClass('active')
				setTimeout(function(){
					$(".mobile-navigation").css("display", 'none')
				},300)
				setTimeout(function(){
					$(".mobile-navigation").css({
						opacity: 0,
						transition: '.5s ease-in-out'
					})
				},100)
				$('.page-name-section').css('paddingTop', '28px')
			}
		})

	});

})(jQuery);