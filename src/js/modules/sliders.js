export  default function slidersInit(){
	
	(function($){
	
		$(document).ready(function(){
			$('.trophies-wrapp').slick({
				infinite: true,
				speed: 300,
				slidesToShow: 4,
				slidesToScroll: 1,
				dots: false,
				arrows: true,
				responsive: [
					{
						breakpoint: 1224,
						settings: {
						  slidesToShow: 3,
						  slidesToScroll: 1,
						  infinite: true,
						  dots: false,
						  arrows: true
						}
					  },
					{
						breakpoint: 768,
						settings: {
						  slidesToShow: 3,
						  slidesToScroll: 1,
						  infinite: true,
						  dots: false,
						  arrows: true
						}
					  },
				  {
					breakpoint: 640,
					settings: {
					  slidesToShow: 2,
					  slidesToScroll: 1,
					  infinite: true,
					  dots: true,
					  arrows: false
					}
				  },
				  {
					breakpoint: 480,
					settings: {
					  slidesToShow: 2,
					  slidesToScroll: 1,
					  dots: true,
					  arrows: false
					}
				  }
				  // You can unslick at a given breakpoint now by adding:
				  // settings: "unslick"
				  // instead of a settings object
				]
			  });
		  });

	})(jQuery);
	
}
 