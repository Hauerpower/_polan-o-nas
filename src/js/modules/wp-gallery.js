export default function galleryInit(){
	(function($){
		
		$('.post-content .gallery').each(function(i){
			$(this).find('.gallery-item').each(function(){
				
				$(this).find('.gallery-icon a').attr('data-fancybox', 'post-gallery-' + i);
				
				if( $(this).find('.wp-caption-text') ){
					var captionText = $(this).find('.wp-caption-text').html();
					$(this).find('.gallery-icon a').attr('data-caption', captionText);
				}
			})
		})
		
	})(jQuery);
}